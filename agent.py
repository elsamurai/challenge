#!/usr/bin/env python3

#Import modules
import subprocess
import json
import requests
import os

# Dictionary to store information
Data = {}

# Operating System Name && Version
def get_osDetails():
	OSData = {}
	normal = subprocess.run(["lsb_release", "-a"],stdout=subprocess.PIPE, stderr=subprocess.PIPE,check=True,text=True)
	outputList = normal.stdout.splitlines()
	for line in outputList:
		nameValue = line.split("\t")
		key = nameValue[0]
		key = key.replace(":","")
		value = nameValue[1]
		OSData[key] = value
	return OSData


# Active session users
def get_users():
	userData = {}
	normal = subprocess.run(["who"],stdout=subprocess.PIPE, stderr=subprocess.PIPE,check=True,text=True)
	outputList = normal.stdout.splitlines()

	iter = 0
	for line in outputList:
		userData[iter] = []
		nameValue = line.split()

		userData[iter].append({
			'username': nameValue[0]
		})
		userData[iter].append({
			'tty': nameValue[1]
		})
		userData[iter].append({
			'loginDate': nameValue[2]
		})
		userData[iter].append({
			'loginTime': nameValue[3]
		})
		if len(nameValue) == 5:
	                userData[iter].append({
        	                'Source': nameValue[4]
	                })
		iter = iter+1
	return userData


# Process information.
def get_cpuinfo():
	CPUData = {}
	normal = subprocess.run(["lscpu"],stdout=subprocess.PIPE, stderr=subprocess.PIPE,check=True,text=True)
	outputList = normal.stdout.splitlines()
	for line in outputList:
		nameValue = line.split(":")
		key = nameValue[0]
		value = nameValue[1].strip()
		CPUData[key] = value
	return CPUData


#USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
#Parser
def get_process():
	procData = {}
	normal = subprocess.run(["ps", "aux"],stdout=subprocess.PIPE, stderr=subprocess.PIPE,check=True,text=True)
	proc = normal.stdout.splitlines()
	iter = 0
	for line in proc[1:]:
		procData[iter] = []
		values = line.split()
		procData[iter].append({
			'username': values[0]
		})
		procData[iter].append({
			'pid': values[1]
		})
		procData[iter].append({
			'cpu': values[2]
		})
		procData[iter].append({
			'mem': values[3]
		})
		procData[iter].append({
			'vsz': values[4]
		})
		procData[iter].append({
			'rss': values[5]
		})
		procData[iter].append({
			'tty': values[6]
		})
		procData[iter].append({
			'stat': values[7]
		})
		procData[iter].append({
			'start': values[8]
		})
		procData[iter].append({
			'time': values[9]
		})

		command = ""
		for commandPart in values[10:]:
			commandPart.strip()
			if command:
				command = command + " " + commandPart
			else:
				command = commandPart
		procData[iter].append({
			'command': command
		})

		iter = iter+1
	return procData


#Hostname
def get_hostname():
	return os.uname()[1]

Data['OSData'] = []
Data['OSData'].append( get_osDetails() )

Data['userData'] = []
Data['userData'].append( get_users() )

Data['CPUData'] = []
Data['CPUData'].append( get_cpuinfo() )

Data['procData'] = []
Data['procData'].append( get_process() )

Data['hostname'] = []
Data['hostname'].append( get_hostname() )

jstr = json.dumps(Data, indent=2)
print(jstr)

#Send data to server
url = 'http://127.0.0.1:9090'
x = requests.post(url, data = jstr)
print (x.text)
