#import modules
from http.server import HTTPServer, BaseHTTPRequestHandler
from io import BytesIO
from json import loads
import time

#Write to a file
def writeFile(input):
	filename = str(time.time()) + ".json"
	f = open(filename, "a")
	f.write(input)
	f.close()

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

	def do_GET(self):
		self.send_response(200)
		self.end_headers()
		self.wfile.write(b'Holis!')

	def do_POST(self):
		content_length = int(self.headers['Content-Length'])
		body = self.rfile.read(content_length)
		self.send_response(200)
		self.end_headers()
		response = BytesIO()
		writeFile( body.decode("utf-8") )


# HTTP Server Listener
httpd = HTTPServer(('localhost', 9090), SimpleHTTPRequestHandler)
httpd.serve_forever()
