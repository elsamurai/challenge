FROM debian:latest
MAINTAINER Ezequiel

RUN apt-get update && apt-get upgrade -yq

RUN mkdir -p /opt/ML

RUN apt-get install -yq \
	python3 \
	git \
	python3-pip
	
RUN pip3 install requests

WORKDIR /opt/ML

RUN git clone https://gitlab.com/elsamurai/challenge.git

RUN apt install software-properties-common -yq

RUN	add-apt-repository 'deb http://cz.archive.ubuntu.com/ubuntu trusty main'

RUN	apt-get update -y --allow-insecure-repositories

RUN apt install lsb-core -yyq --allow-unauthenticated